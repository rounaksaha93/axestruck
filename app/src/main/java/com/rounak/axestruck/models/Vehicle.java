package com.rounak.axestruck.models;

/**
 * Created by rounak on 04/10/16.
 */
public class Vehicle {
    String Vehicle;
    String Lati;
    String Long;
    String Date;
    String Imei;
    String VehicleId;
    String Location;
    String Moving;
    String NoDate;
    String CompanyId;
    String TrackNum;
    String ExtraInfo;
    String DriverName;
    String DriverMobile;
    String Mobile;
    String Device;
    String IdleTime;
    String FltSpeed;
    String PlanCode;
    String SiteDist;

    public String getVehicle() {
        return Vehicle;
    }

    public String getSiteDist() {
        return SiteDist;
    }

    public String getPlanCode() {
        return PlanCode;
    }

    public String getFltSpeed() {
        return FltSpeed;
    }

    public String getIdleTime() {
        return IdleTime;
    }

    public String getDevice() {
        return Device;
    }

    public String getMobile() {
        return Mobile;
    }

    public String getDriverMobile() {
        return DriverMobile;
    }

    public String getDriverName() {
        return DriverName;
    }

    public String getExtraInfo() {
        return ExtraInfo;
    }

    public String getTrackNum() {
        return TrackNum;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public String getNoDate() {
        return NoDate;
    }

    public String getMoving() {
        return Moving;
    }

    public String getLocation() {
        return Location;
    }

    public String getVehicleId() {
        return VehicleId;
    }

    public String getImei() {
        return Imei;
    }

    public String getLong() {
        return Long;
    }

    public String getDate() {
        return Date;
    }

    public String getLati() {
        return Lati;
    }


}
