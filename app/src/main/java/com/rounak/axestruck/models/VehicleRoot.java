package com.rounak.axestruck.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rounak on 04/10/16.
 */
public class VehicleRoot {
    @SerializedName("array")
    public ArrayList<Vehicle> vehicles = new ArrayList<>();
}
