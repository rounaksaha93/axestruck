package com.rounak.axestruck.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.rounak.axestruck.R;
import com.rounak.axestruck.db.DatabaseHandler;
import com.rounak.axestruck.db.InsertHelper;
import com.rounak.axestruck.models.Vehicle;
import com.rounak.axestruck.models.VehicleRoot;
import com.rounak.axestruck.network.ApiManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity{

    ApiManager api = new ApiManager();
    Button butJson;
    private ArrayList<Vehicle> data;
    DatabaseHandler dbh;
    int flag = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbh = new DatabaseHandler(this);
        //Test to see if data has been inserted succesfully
        dbh.getAllVehicle();
        butJson = (Button) findViewById(R.id.butJson);
        butJson.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                loadJSON();
            }
        });
    }

    private void loadJSON() {

        api.service.getVehicles().enqueue(new Callback<VehicleRoot>() {
            @Override
            public void onResponse(Call<VehicleRoot> call, Response<VehicleRoot> response) {
                VehicleRoot jsonResponse = response.body();
                data = jsonResponse.vehicles;
                dataInsert();
            }

            @Override
            public void onFailure(Call<VehicleRoot> call, Throwable t) {
                Toast.makeText(MainActivity.this, R.string.jsonFail, Toast.LENGTH_SHORT).show();
                Log.e("Failure Message : ", t.getMessage());
            }
        });
    }





    void dataInsert()
    {
        int start = 0;
        int end = 100;
        while(end<=data.size())
        {
                InsertHelper insHelper = new InsertHelper(data,dbh,start,end);
                Log.e("DB Op - ", "Start Thread");
                insHelper.start();
                start = end;
                end  = end + 100;
                if(end>data.size() && flag == 0)
                {
                    end = data.size();
                    flag = 1;
                }
        }
     }

}
