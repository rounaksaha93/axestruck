package com.rounak.axestruck.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.widget.Toast;

import com.rounak.axestruck.R;
import com.rounak.axestruck.models.Vehicle;
import com.rounak.axestruck.models.VehicleRoot;
import com.rounak.axestruck.network.ApiManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rounak on 05/10/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {


    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "axestruck";

    // Vehicles table name
    private static final String TABLE_VehicleList = "vehicleList";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
     public void onCreate(SQLiteDatabase db) {
        String CREATE_VehicleList_TABLE = "CREATE TABLE " + TABLE_VehicleList + " (" +
                "Order_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " vehicleID INTEGER," +
                " vehicleWebID TEXT," +
                " vehicleName TEXT," +
                " date TEXT," +
                " mainMessage TEXT," +
                " subMessage TEXT," +
                " latitude TEXT," +
                " longitude TEXT," +
                " Imei INTEGER," +
                " Location TEXT," +
                " Moving INTEGER," +
                " NoDate INTEGER," +
                " CompanyId INTEGER," +
                " TrackNum TEXT," +
                " ExtraInfo TEXT," +
                " MobileNoOfDevice TEXT," +
                " GpsDeviceType TEXT," +
                " DriverMobileNumber TEXT," +
                " IdleTime TEXT," +
                " fltSpeed TEXT)";
        db.execSQL(CREATE_VehicleList_TABLE);
    }

    // Upgrading database
    @Override
     public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VehicleList);
        // Create tables again
        onCreate(db);
    }

     synchronized public void addVehicle(Vehicle vehicle) {
         //Opening the connection here
         SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleId", vehicle.getVehicleId()); // Vehicle ID
        values.put("vehicleWebID", vehicle.getVehicle()); //Vehicle Web ID
        values.put("vehicleName", ""); //Vehicle Name - Not specified in JSON attributes. Hence, left blank
        values.put("date", vehicle.getDate()); //Date
        values.put("mainMessage", ""); //Main Message - Not specified in JSON attributes. Hence, left blank
        values.put("subMessage", ""); //Sub Message - Not specified in JSON attributes. Hence, left blank
        values.put("latitude", vehicle.getLati()); //Lat
        values.put("longitude", vehicle.getLong()); //Long
        values.put("Imei", vehicle.getImei()); //IMEI
        values.put("Location", vehicle.getLocation()); //Location
        values.put("NoDate", vehicle.getNoDate()); //NoDate
        values.put("CompanyId", vehicle.getCompanyId()); //Company ID
        values.put("ExtraInfo", vehicle.getExtraInfo()); //Extra Info
        values.put("TrackNum", vehicle.getTrackNum()); //Track Num
        values.put("MobileNoOfDevice", vehicle.getMobile()); //Mobile Number of Device
        values.put("GpsDeviceType", vehicle.getDevice()); //Device Type
        values.put("DriverMobileNumber", vehicle.getDriverMobile()); //Drive Mobile Number
        values.put("IdleTime", vehicle.getIdleTime()); //Idle Time
        values.put("fltSpeed", vehicle.getFltSpeed()); //FLT Speed


        // Inserting Row
        try
        {
            db.insert(TABLE_VehicleList, null, values);
        }
        catch (Exception e)
        {
            Log.e("DB Insert Error : ", e.getMessage());
        }

        DatabaseManager.getInstance().closeDatabase(); // Closing database connection
    }

    //Use this method to test if entry has been made successfully
    public void getAllVehicle() {
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_VehicleList;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String vID = cursor.getString(0);
                Log.e("Vehicle ID : ", String.valueOf(vID));
             } while (cursor.moveToNext());
        }
    }
}