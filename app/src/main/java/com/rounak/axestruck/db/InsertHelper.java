package com.rounak.axestruck.db;

import android.content.Context;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.rounak.axestruck.activities.MainActivity;
import com.rounak.axestruck.models.Vehicle;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by rounak on 06/10/16.
 */
public class InsertHelper extends Thread {

    ArrayList<Vehicle> vehicles;
    DatabaseHandler dbh;
    int start, end;
    public InsertHelper(ArrayList<Vehicle> vehicles, DatabaseHandler dbh, int start, int end){
        this.vehicles = vehicles;
        this.dbh  = dbh;
        this.start = start;
        this.end = end;
    }
    public void run(){
        insertNow();
    }

    synchronized public void insertNow(){
        for(int i=start;i<end;i++){
            dbh.addVehicle(vehicles.get(i));
        }
        Log.e("DB Op - ", "End Thread");
    }


}
