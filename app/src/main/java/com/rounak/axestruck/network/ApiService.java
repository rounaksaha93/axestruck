package com.rounak.axestruck.network;



import com.rounak.axestruck.models.Vehicle;
import com.rounak.axestruck.models.VehicleRoot;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @GET("vehicle.php")
    Call<VehicleRoot> getVehicles();
}
