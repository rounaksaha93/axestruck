package com.rounak.axestruck;

import android.app.Application;
import android.database.sqlite.SQLiteOpenHelper;

import com.rounak.axestruck.db.DatabaseHandler;
import com.rounak.axestruck.db.DatabaseManager;

/**
 * Created by rounak on 09/10/16.
 */
public class AppMain extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseManager.initializeInstance(new DatabaseHandler(getApplicationContext()));
    }
}
